# CHANGELOG

## 1.0.5 (  )

* [`docker`](./roles/docker/) added defaults for `docker_resources`,
  `docker_resources_types`, `docker_resource_groups_disabled`.
* Upgrade naar `community.docker` `4.4.0`.
* [`docker`](./roles/docker/) add missing fact / variable
  `docker_container_copy_into`.
* [`docker`](./roles/docker/) new variables `docker_resources` as alias for
  `linux_resources` from `c2platform.core.linux` role.
* [`microk8s`](./roles/microk8s) replaced `microk8s_files` with
  `microk8s_resources`, `microk8s_linux_resources` and
  `microk8s_linux_bootstrap_resources`.
* [`kubernetes`](./roles/kubernetes) major refactoring which new variables
  `kubernetes_resources`, `kubernetes_linux_resources` and
  `kubernetes_linux_bootstrap_resources`.
* [`apache`](./roles/apache/) added `apache_resource_groups_disabled`.
* [`haproxy`](./roles/haproxy) integrated `c2platform.core.linux` role which
  introduces new variables `haproxy_resources`, `haproxy_resources_types` and
  `haproxy_resource_groups_disabled`.
* [`tomcat`](./roles/tomcat) major refactoring of the role to use
  `tomcat_resources`.

## 1.0.4 ( 2024-11-13 )

* [`docker`](./roles/docker/) `docker_compose` deprecated, updated to
  `docker_compose_v2`.
* `c2platform.core` → `1.0.23`.
* [`apache`](./roles/apache/) added tags `install`, `config`, `application`,
  `apache`.

## 1.0.3 ( 2024-06-26 )

* `c2platform.core` → `1.0.22`.
* `.gitlab-ci.yml` based on `c2platform.core`.
* Fix filter doc.
* [`apache`](./roles/apache/) added `apache_resources_types`.
* [`exim4`](./roles/exim4/) new role `exim4`.

## 1.0.2 ( 2024-03-08 )

* [`apache`](./roles/apache/) added `apache_resources`.
* Removed `role reverse_proxy`.

## 1.0.1 ( 2023-10-09 )

* Removed role `postfix`.
* [`docker`](./roles/docker) added `docker_container_copy_into`.
* Added dependency `community.docker` `3.4.8`.
* [`kubernetes`](./roles/kubernetes/) extra `retries` and `delay` attributes for `k8s` module.
* [`docker`](./roles/docker) added `hostname` and `sysctls`.
* [`kubernetes`](./roles/kubernetes/) added new type `service_uri` so we can wait
  for services to become available.
* Removed role `https_proxy`.
* Remove role `mail`.

## 1.0.0 ( 2023-06-16 )

* [`microk8s`](./roles/microk8s) configurable version via new default vars `microk8s_version` and `microk8s_classic` default values `1.26/stable`, `true`.

## 0.1.18 ( 2023-04-24 )

* [`kubernetes`](./roles/kubernetes) simplified.

## 0.1.17 ( 2023-04-05 )

* [`microk8s`](./roles/microk8s) new role.
* [`kubernetes`](./roles/kubernetes) new role.

## 0.1.16 ( 2023-04-03 )

* [`dnsmasq`](./roles/dnsmasq) new role.

## 0.1.15 ( 2022-10-04 )

* [`keycloak`](./roles/keycloak) new role, migrated from [GitHub](https://github.com/c2platform/ansible-role-keycloak).
* [`keycloak`](./roles/keycloak) changed / improved to use [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/README.md).

## 0.1.14 ( 2022-10-02 )

* [`apache`](./roles/apache) added `apache_owner_settings` for Debian, Red Hat
* [`haproxy`](./roles/haproxy) Defaults vars `haproxy_files`, `haproxy_directories`, `haproxy_acl`, `haproxy_unarchive` and `haproxy_disabled` for[c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files).
* [`apache`](./roles/apache) Defaults vars `apache_files`, `apache_directories`, `apache_acl`, `apache_unarchive` and `apache_disabled` for[c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files).
* [`tomcat`](./roles/tomcat) new role in this collection based on source from [ansible-role-tomcat](https://gitlab.com/c2platform/ansible-role-tomcat).

## 0.1.12 (2022-05-20)

* Bug fix apache role

## 0.1.11 (2022-05-13)

* Enhanched docker role

## 0.1.10 (2022-05-04)

* Enhanched docker role

## 0.1.9 (2022-04-24)

* Enhanched docker role

## 0.1.8 (2022-04-24)

* Enhanched docker role

## 0.1.7 (2022-04-01)

* Fix HAProxy. Gather facts using [c2platform.core.facts](https://gitlab.com/c2platform/ansible-collection-core/tree/master/roles/facts) only when `haproxy_facts_gather_hosts`

## 0.1.6 (2022-03-30)

* HAProxy using [c2platform.core.facts](https://gitlab.com/c2platform/ansible-collection-core/tree/master/roles/facts) - configure `haproxy_facts_gather_hosts` `haproxy_facts_filter` to get `haproxy_facts_hosts`.

## 0.1.5 (2022-03-28)

* Fix apache role. Var `common_files_role_name`.

## 0.1.2 (2022-03-08)

* Reusing / integrating [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/blob/master/roles/files) to provide dicts apache_files, apache_directories, apache_acl

## 0.1.0 (2022-02-14)

* initial role
