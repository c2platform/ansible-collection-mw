# Ansible Role c2platform.mw.exim4

This Ansible role for
[Exim Internet Mailer](https://www.exim.org/)
is a complete solution to install and configure the Exim4 email server on Linux
systems. The role supports multiple operating systems, including RHEL, CentOS,
Debian, Ubuntu, and others.

<!-- MarkdownTOC levels="2,3" autolink="true" -->

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`exim4_resources`](#exim4_resources)
- [Handlers](#handlers)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

<!-- /MarkdownTOC -->

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `exim4_resources`

Currently this role has only one variable `exim4_resources` ( which is alias for
`linux_resources` ). This variable is used to configure the installation of the
`exim4` package and subsequently create the file
`/etc/exim4/update-exim4.conf.conf`.

This role inherits from `c2platform.core.linux` Ansible role, so please refer to
that role's documentation for more information on what you can do with the
`exim4_resources` variable.

## Handlers

<!-- This section describes the handlers used by the role. Handlers are actions triggered by tasks and notified when changes occur. Explain what the handlers do and when they are invoked. -->

| Name                      | Listeners                                   | Description                                           |
|---------------------------|---------------------------------------------|-------------------------------------------------------|
| **Reconfigure Exim4**     | `update-exim4.conf`, `update-restart-exim4` | This handler will run the command `update-exim4.conf` |
| **Restart Exim4 service** | `restart-exim4`, `update-restart-exim4`     | This handler restart the `exim4` service.             |


## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: Exim4
  hosts: exim4
  become: yes

  roles:
    - { role: c2platform.core.common, tags: ["common"] }
    - { role: c2platform.mw.exim4, tags: ["exim4"] }

  vars:
    exim4_resources:
        2-config:
            - name: update-exim4.conf.conf
            type: copy
            dest: /etc/exim4/update-exim4.conf.conf
            content: |
                dc_eximconfig_configtype='internet'
                dc_other_hostnames='c2d-exim4 ; test.example.com'
                dc_local_interfaces='127.0.0.1 ; ::1 ; 1.1.6.127'
                dc_readhost=''
                dc_relay_domains='example.com ; example.net'
                dc_minimaldns='false'
                dc_relay_nets='1.1.6.128/24 ; 1.1.6.129/24'
                dc_smarthost=''
                CFILEMODE='644'
                dc_use_split_config='false'
                dc_hide_mailname=''
                dc_mailname_in_oh='true'
                dc_localdelivery='mail_spool'
            owner: root
            group: root
            mode: '644'
            notify: update-restart-exim4
            - name: 00_local_settings
            type: copy
            dest: /etc/exim4/conf.d/main/00_local_settings
            content: |
                MAIN_TLS_ENABLE=true
            owner: root
            group: root
            mode: '644'
            notify: update-restart-exim4
            - name: Enable SMTP authentication
            path: /etc/exim4/exim4.conf.template
            type: blockinfile
            marker: "# {mark} ANSIBLE MANAGED BLOCK for plain_server/login_server"
            insertafter: "begin authenticators"
            block: |
                plain_server:
                driver = plaintext
                public_name = PLAIN
                server_condition = "${if crypteq{$auth3}{${extract{1}{:}{${lookup{$auth2}lsearch{CONFDIR/passwd}{$value}{*:*}}}}}{1}{0}}"
                server_set_id = $auth2
                server_prompts = :
                .ifndef AUTH_SERVER_ALLOW_NOTLS_PASSWORDS
                server_advertise_condition = ${if eq{$tls_in_cipher}{}{}{*}}
                .endif
                login_server:
                driver = plaintext
                public_name = LOGIN
                server_prompts = "Username:: : Password::"
                server_condition = "${if crypteq{$auth2}{${extract{1}{:}{${lookup{$auth1}lsearch{CONFDIR/passwd}{$value}{*:*}}}}}{1}{0}}"
                server_set_id = $auth1
                .ifndef AUTH_SERVER_ALLOW_NOTLS_PASSWORDS
                server_advertise_condition = ${if eq{$tls_in_cipher}{}{}{*}}
                .endif
            notify: update-restart-exim4
            - name: Listen ports 587 and 25
            path: /etc/default/exim4
            type: lineinfile
            regexp: ^(SMTPLISTENEROPTIONS=|#SMTPLISTENEROPTIONS=).*
            line: SMTPLISTENEROPTIONS='-oX 587:25 -oP /var/run/exim4/exim.pid'
            notify: update-restart-exim4
```