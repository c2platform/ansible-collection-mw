# Ansible Role docker

This Ansible role facilitates the creation of Docker resources using the
[community.docker](https://docs.ansible.com/ansible/latest/collections/community/docker/index.html)
Ansible collection

<!-- MarkdownTOC levels="2,3" autolink="true" -->

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`docker_networks`](#docker_networks)
  - [`docker_images`](#docker_images)
  - [`docker_containers`](#docker_containers)
  - [`docker_container_copy_into`](#docker_container_copy_into)
  - [`docker_containers_changed`](#docker_containers_changed)
  - [`docker_resources`](#docker_resources)
- [Handlers](#handlers)
  - [`Restart docker`](#restart-docker)
  - [`Restart all containers`](#restart-all-containers)
  - [`Restart changed containers`](#restart-changed-containers)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

<!-- /MarkdownTOC -->

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->



## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `docker_networks`

```yaml
docker_networks:
  - name: network_one
    connected:
      - container_a
      - container_b
      - container_c
```

View details on the
[docker_network module](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_network_module.html#ansible-collections-community-docker-docker-network-module)
module.

### `docker_images`

```yaml
docker_images:
  - name: pacur/centos-7
    source: pull
    # Select platform for pulling. If not specified, will pull whatever docker prefers.
    pull:
      platform: amd64
```

View details on the
[docker_image module](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_image_module.html#ansible-collections-community-docker-docker-image-module)
module.

### `docker_containers`

```yaml
docker_images:
  - name: mydata
    image: busybox
    volumes:
      - /data
```

View detais on the [docker_container module](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_container_module.html#ansible-collections-community-docker-docker-container-module)
module.

### `docker_container_copy_into`

```yaml
docker_container_copy_into:
  - container: matomo
    container_path: /var/www/html/config/config.ini.php
    content: "{{ ok_matomo_config['content'] }}"
    mode: 0755
    # notify: Restart all containers
```
View details on the
[community.docker.docker_container_copy_into](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_container_copy_into_module.html)
module.

### `docker_containers_changed`

The `docker_containers_changed` list variable, which is empty by default, is
utilized by tasks in this role to identify containers that have changed. This
list is used, for example, to trigger restarts.

```yaml
docker_containers_changed: []
```

### `docker_resources`

Use variable `docker_resources` to provision any Linux resources provided by the
`c2platform.core.linux` role.

## Handlers

### `Restart docker`

This handler restarts the Docker service using the `ansible.builtin.service`
module.

### `Restart all containers`

This handler restarts all containers defined in the `docker_containers` list using
the `ansible.builtin.service` module.

### `Restart changed containers`

This handler restarts all containers defined in the `docker_containers_changed`
list using the `ansible.builtin.service` module. This list is populated when files
copied into a container using `docker_container_copy_into` change in the
container.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```