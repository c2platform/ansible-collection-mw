# Ansible Role c2platform.mw.tomcat

The `c2platform.mw.tomcat` Ansible role is designed to install, configure, and
manage [Apache Tomcat](https://tomcat.apache.org/) servers on Linux hosts. This
document provides details about the role's requirements, variables,
dependencies, and usage.

- [Requirements](#requirements)
  - [Java](#java)
- [Role Variables](#role-variables)
  - [Tomcat Home and Version](#tomcat-home-and-version)
  - [Tomcat Service](#tomcat-service)
  - [Tomcat Resources](#tomcat-resources)
    - [Configuration from Git](#configuration-from-git)
    - [PAM Limits](#pam-limits)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)
- [Notes](#notes)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

### Java

Tomcat requires Java to be installed on the host system. It is recommended to
use the Ansible Java role
[`c2platform.core.java`](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/java)
to handle the Java installation. See the [Example Playbook](#example-playbook)
for guidance.

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### Tomcat Home and Version

The installation directory and version of Tomcat are controlled by the variables
`tomcat_home` and `tomcat_version`. From these, `tomcat_home_version` and
`tomcat_home_link` are derived.

- `tomcat_home_version` specifies where the specific version of Tomcat will be
  installed.
- `tomcat_home_link` creates a symbolic link to indicate the current active
  Tomcat version, facilitating easy version switching.

| Variable               | Example Value               |
|------------------------|-----------------------------|
| `tomcat_home`          | `/opt/tomcat`               |
| `tomcat_version`       | `9.0.67`                    |
| `tomcat_home_version`  | `/opt/tomcat/tomcat-9.0.67` |
| `tomcat_home_link`     | `/opt/tomcat/tomcat`        |
| `tomcat_user`          | `tomcat`                    |
| `tomcat_group`         | `tomcat`                    |
| `tomcat_home_dir_mode` | `'0750'`                    |

The tree output below illustrates the directory structure of a sample Tomcat
installation:

```bash
root@gsd-tomcat1:/# tree -L 2 /opt/tomcat/
/opt/tomcat/
├── conf
│   ├── c2-gsd-tomcat1.crt
│   └── c2-gsd-tomcat1.key
├── tomcat -> /opt/tomcat/tomcat-9.0.67
└── tomcat-9.0.67
    ├── bin
    ├── BUILDING.txt
    ├── conf
    ├── CONTRIBUTING.md
    ├── lib
    ├── LICENSE
    ├── logs
    ├── NOTICE
    ├── README.md
    ├── RELEASE-NOTES
    ├── RUNNING.txt
    ├── temp
    ├── webapps
    └── work
```

### Tomcat Service

The Tomcat service configuration is managed using the `tomcat_service`
dictionary and variables with the `tomcat_service_` prefix. By default, the role
creates a system service file located at `/etc/systemd/system/tomcat.service`.

```text
#
# This file is managed by Ansible; all modifications will be overwritten.
#

[Unit]
Description=Apache Tomcat Web Server

[Service]
User=tomcat
Group=tomcat
ExecStart=/bin/bash /opt/tomcat/tomcat-9.0.67/bin/catalina.sh run
LimitNOFILE=131072

[Install]
WantedBy=multi-user.target
```

### Tomcat Resources

Additional configuration of the Tomcat server is achieved via the versatile
`tomcat_resources` variable. This variable integrates with the
`c2platform.core.linux` role, allowing management of a wide range of resources
using various Ansible modules. For detailed information, refer to the
[documentation](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/linux)
of this role and see the [Example Playbook](#example-playbook).

The number of use cases supported by `tomcat_resources` is extensive. To
illustrate, consider two examples below. A more comprehensive example, including
main configuration files like `server.xml` and `tomcat-users.xml`, can be found
in the [Example Playbook](#example-playbook).

#### Configuration from Git

If parts of your Tomcat application's configuration are stored in a Git
repository, `tomcat_resources` can help you fetch the configuration from Git.
Below is an example configuration that is also part of the
[Example Playbook](#example-playbook). Check
[`group_vars/tomcat_linux/helloworld.yml`](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/group_vars/tomcat_linux/helloworld.yml):

```yaml
tomcat_apps:
  helloworld:
    - name: helloworld
      source: https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war
      filename: helloworld.war
      notify: restart tomcat instance

tomcat_resources:
  helloworld:
    - name: Get config from GIT
      type: git
      repo: https://github.com/c2platform/tomcat-git-config
      dest: /tmp/tomcat_git_config
      update: true
    - name: Configure helloworld App
      type: copy
      remote_src: true
      src: /tmp/tomcat_git_config/myapp.properties
      dest: "{{ tomcat_home_version }}/conf/helloworld.properties"
      notify: restart tomcat instance
```

#### PAM Limits

You can also manage Linux PAM limits for Tomcat:

```yaml
tomcat_resources:
  core:
    - name: Manage PAM Limits
      type: pam_limits
      defaults:
        limit_item: nofile
        domain: "{{ tomcat_user }}"
        notify: restart tomcat instance
      resources:
        - limit_type: soft
          value: 65537
        - limit_type: hard
          value: 131072
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->
[`c2platform.core`](https://galaxy.ansible.com/c2platform/core) for some of its
operations.

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->
in the open-source
[GIS Platform Reference Implementation](https://gitlab.com/c2platform/rws/ansible-gis).
See
[`plays/mw/tomcat.yml`](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/plays/mw/tomcat.yml?ref_type=heads)
and
[`group_vars/tomcat_linux/`](https://gitlab.com/c2platform/rws/ansible-gis/-/tree/master/group_vars/tomcat_linux?ref_type=heads).
Additionally, review the How-To document
[Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and
Windows Hosts](https://c2platform.org/docs/howto/rws/certs/).

## Notes

For a discussion on the differences between CATALINA_OPTS and JAVA_OPTS, see
[this Stack Overflow
thread](https://stackoverflow.com/questions/11222365/catalina-opts-vs-java-opts-what-is-the-difference).