# Ansible Role c2platform.mw.microk8s

A brief description of the role goes here.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [Version](#version)
  - [`microk8s_linux_resources` and `microk8s_linux_bootstrap_resources`](#microk8s_linux_resources-and-microk8s_linux_bootstrap_resources)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### Version

To configure the version to install use `microk8s_version` and `microk8s_classic` as shown below:

```yaml
microk8s_version: 1.26/stable
microk8s_classic: true
```
### `microk8s_linux_resources` and `microk8s_linux_bootstrap_resources`

Additional configuration is achieved via the versatile
`microk8s_linux_resources` and `microk8s_linux_bootstrap_resources`
variable. These variable integrate the `c2platform.core.linux` role, allowing
management of a wide range of resources using various Ansible modules. For
detailed information, refer to the
[documentation](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/linux)
of this role and see the [Example Playbook](#example-playbook).

The difference between the two is the order of processing.

1. First `microk8s_linux_bootstrap_resources` is processed.
2. MicroK8s is installed and configured.
3. Finaly  `microk8s_linux_resources` is processed.

See the role
[`kubernetes`](../kubernetes/README.md)
for an example demonstrating the use of these variables.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See [plays/mw/mk8s.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mw/mk8s.yml) and [group_vars/mk8s/main.yml](https://gitlab.com/c2platform/ansible/-/tree/master/group_vars/mk8s/main.yml)

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```