# Ansible Role c2platform.mw.dnsmasq

Install and configure [dnsmasq](https://dnsmasq.org/) on your system.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [Configuration](#configuration)
  - [Files, directories and ACL](#files-directories-and-acl)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### Configuration

Manage configuration in `/etc/dnsmasq.conf` using var `dnsmasq_configuration` for example:

```yaml
dnsmasq_configuration: |
  port={{ dnsmasq_port }}
  listen-address=1.1.4.205
  domain=c2platform.org
  address=/.c2platform.org/1.1.4.205
```

### Files, directories and ACL

Use dict `dnsmasq_files` to create / manage any other files see [c2platform.core.files](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/files). For example a `trust-anchors.conf` file.


## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See play [c2platform/ansible/plays/mw/dnsmasq.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mw/dnsmasq.yml) and related configuration in [c2platform/ansible/group_vars/dnsmasq/main.yml](https://gitlab.com/c2platform/ansible/-/blob/master/group_vars/dnsmasq/main.yml)
