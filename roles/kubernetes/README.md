# Ansible Role c2platform.mw.kubernetes

Manage Kubernetes using
[`kubernetes.core`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/index.html)
collection through one easy, flexible and powerful `kubernetes_resources`
variable.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`kubernetes_<module_name>`](#kubernetes_module_name)
  - [`kubernetes_resources`](#kubernetes_resources)
    - [`get_url`: Downloads files from HTTP, HTTPS, or FTP to node](#get_url-downloads-files-from-http-https-or-ftp-to-node)
    - [`helm`: Manages Kubernetes packages with the Helm package manager](#helm-manages-kubernetes-packages-with-the-helm-package-manager)
    - [`helm_info`: Get information from Helm package deployed inside the cluster](#helm_info-get-information-from-helm-package-deployed-inside-the-cluster)
    - [`helm_plugin`: Manage Helm plugins](#helm_plugin-manage-helm-plugins)
    - [`helm_plugin_info`: Gather information about Helm plugins](#helm_plugin_info-gather-information-about-helm-plugins)
    - [`helm_repository`: Manage Helm repositories.](#helm_repository-manage-helm-repositories)
    - [`helm_template`: Render chart templates](#helm_template-render-chart-templates)
    - [`k8s`: Manage Kubernetes (K8s) objects](#k8s-manage-kubernetes-k8s-objects)
    - [`k8s_cluster_info`: Describe Kubernetes (K8s) cluster, APIs available and their respective versions](#k8s_cluster_info-describe-kubernetes-k8s-cluster-apis-available-and-their-respective-versions)
    - [`k8s_cp`: Copy files and directories to and from pod.](#k8s_cp-copy-files-and-directories-to-and-from-pod)
    - [`k8s_drain`: Drain, Cordon, or Uncordon node in k8s cluster](#k8s_drain-drain-cordon-or-uncordon-node-in-k8s-cluster)
    - [`k8s_exec`: Execute command in Pod](#k8s_exec-execute-command-in-pod)
    - [`k8s_info`: Describe Kubernetes (K8s) objects](#k8s_info-describe-kubernetes-k8s-objects)
    - [`k8s_json_patch`: Apply JSON patch operations to existing objects](#k8s_json_patch-apply-json-patch-operations-to-existing-objects)
    - [`k8s_log`: Fetch logs from Kubernetes resources](#k8s_log-fetch-logs-from-kubernetes-resources)
    - [`k8s_rollback`: Rollback Kubernetes (K8S) Deployments and DaemonSets](#k8s_rollback-rollback-kubernetes-k8s-deployments-and-daemonsets)
    - [`k8s_scale`: Set a new size for a Deployment, ReplicaSet, Replication Controller, or Job.](#k8s_scale-set-a-new-size-for-a-deployment-replicaset-replication-controller-or-job)
    - [`k8s_service`: Manage Services on Kubernetes](#k8s_service-manage-services-on-kubernetes)
    - [`k8s_taint`: Taint a node in a Kubernetes/OpenShift cluster](#k8s_taint-taint-a-node-in-a-kubernetesopenshift-cluster)
    - [`reboot`: Reboot a machine](#reboot-reboot-a-machine)
    - [`script`: Runs a local script on a remote node after transferring it](#script-runs-a-local-script-on-a-remote-node-after-transferring-it)
    - [`shell`: Execute shell commands on targets](#shell-execute-shell-commands-on-targets)
    - [`uri`: Interacts with webservices](#uri-interacts-with-webservices)
    - [`wait_for_connection`: Waits until remote system is reachable/usable](#wait_for_connection-waits-until-remote-system-is-reachableusable)
  - [`kubernetes_linux_resources` and `kubernetes_linux_bootstrap_resources`](#kubernetes_linux_resources-and-kubernetes_linux_bootstrap_resources)
  - [Type `service_uri`](#type-service_uri)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

[kubernetes.core](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/index.html) requires Python [kubernetes](https://pypi.org/project/kubernetes/) package. In c2d reference project this is configured to be installed using a project variable `c2_bootstrap_packages_pip_extra`. See also [group_vars/mk8s/main.yml](https://gitlab.com/c2platform/ansible/-/tree/master/group_vars/mk8s/main.yml).

```yaml
c2_bootstrap_packages_pip_extra:
  - kubernetes==26.1.0
```

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `kubernetes_<module_name>`

### `kubernetes_resources`

<!-- start supported_modules -->
This roles supports a selection of 24 modules
from the collections `ansible.builtin`, `kubernetes.core`. This section
contains simple examples on how these modules can be used via the `kubernetes_resources`.


#### `get_url`: Downloads files from HTTP, HTTPS, or FTP to node

The `get_url` module in Ansible is used to download files from HTTP, HTTPS, or FTP
sources to a node. It's particularly useful for ensuring nodes have the correct
files before software installations or configurations occur. Below are some examples
showcasing how to use this module with the `c2platform.core.kubernetes` role:

```yaml
kubernetes_resources:
  downloads:
    - name: Download foo.conf
      module: get_url
      url: http://example.com/path/file.conf
      dest: /etc/foo.conf
      mode: '0440'

    - name: Download file and force basic auth
      module: get_url
      url: http://example.com/path/file.conf
      dest: /etc/foo.conf
      force_basic_auth: yes

    - name: Download file with custom HTTP headers
      module: get_url
      url: http://example.com/path/file.conf
      dest: /etc/foo.conf
      headers:
        key1: one
        key2: two
```

For more detailed documentation on the `ansible.builtin.get_url` module, please refer to
[`ansible.builtin.get_url`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html).

#### `helm`: Manages Kubernetes packages with the Helm package manager

The `helm` module in Ansible provides a way to manage Kubernetes packages using
the Helm package manager. It supports various operations like installing, upgrading,
and deleting Helm charts in your Kubernetes clusters. Below are some examples demonstrating
how you can use this module within the `c2platform.core.kubernetes` role, leveraging
the flexibility of the "resources" variable.

Example: Deploy the latest version of the Prometheus chart inside the monitoring
namespace and create the namespace if it does not exist.

```yaml
kubernetes_resources:
  helm_packages:
    - name: Deploy Prometheus
      module: helm
      release_name: test
      chart_ref: stable/prometheus
      release_namespace: monitoring
      create_namespace: true
```

Example: Deploy the Grafana chart with specific values to customize replicas.

```yaml
kubernetes_resources:
  helm_packages:
    - name: Deploy Grafana with custom values
      module: helm
      release_name: test
      chart_ref: stable/grafana
      release_namespace: monitoring
      release_values:
        replicas: 2
```

Example: Grouping Helm package deployments with common settings using `defaults`.

```yaml
kubernetes_resources:
  helm_packages:
    - name: Deploy with common settings
      module: helm
      defaults:
        release_namespace: monitoring
        update_repo_cache: true
      resources:
        - release_name: deploy_grafana
          chart_ref: stable/grafana
          release_values:
            replicas: 2
        - release_name: deploy_prometheus
          chart_ref: stable/prometheus
```

For more detailed documentation on the `kubernetes.core.helm` module, please refer to
[`kubernetes.core.helm`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/helm_module.html).

#### `helm_info`: Get information from Helm package deployed inside the cluster

The `helm_info` module allows you to gather detailed information about Helm
packages deployed in a Kubernetes cluster. This includes values, states, app
versions, and more pertinent data about the Helm releases. Below are examples
of how you can configure this module to extract such data, showcasing the
benefits of using Ansible's "resources" variable for more structured and DRY
YAML configuration.

```yaml
kubernetes_resources:
  helm_info:
    - name: Gather information of Grafana chart inside monitoring namespace
      module: helm_info
      resources:
        - release_name: test
          release_namespace: monitoring

# Example showcasing specifying release state
kubernetes_resources:
  helm_resources:
    - name: Gather information about test-chart with pending state
      module: helm_info
      resources:
        - release_name: test-chart
          release_namespace: testenv
          release_state:
            - pending

# Advanced Example using `defaults` to DRY up configuration
kubernetes_resources:
  helm_info:
    - name: Gather Helm releases information
      module: helm_info
      defaults:
        release_namespace: common
      resources:
        - release_name: release1
        - release_name: release2
          release_state:
            - all
```

For more detailed documentation on the `kubernetes.core.helm_info` module,
please refer to
[`kubernetes.core.helm_info`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/helm_info_module.html).

#### `helm_plugin`: Manage Helm plugins

The `helm_plugin` module allows for the management of Helm plugins through Ansible.
It provides capabilities to install, update, and remove Helm plugins using paths
from either a local filesystem or a remote VCS repository.

Here are some examples demonstrating the role `c2platform.core.kubernetes` and
how the `helm_plugin` module can be configured:

```yaml
kubernetes_resources:
  helm_plugins:
    - name: Install Helm env plugin
      module: helm_plugin
      plugin_path: https://github.com/adamreese/helm-env
      state: present

    - name: Install Helm plugin from local filesystem
      module: helm_plugin
      plugin_path: https://domain/path/to/plugin.tar.gz
      state: present

    - name: Remove Helm env plugin
      module: helm_plugin
      plugin_name: env
      state: absent

    - name: Install Helm plugin with a specific version
      module: helm_plugin
      plugin_version: 2.0.1
      plugin_path: https://domain/path/to/plugin.tar.gz
      state: present

    - name: Update Helm plugin
      module: helm_plugin
      plugin_name: secrets
      state: latest
```

For more detailed documentation on the `kubernetes.core.helm_plugin` module,
please refer to
[`kubernetes.core.helm_plugin`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/helm_plugin_module.html).

#### `helm_plugin_info`: Gather information about Helm plugins

The `helm_plugin_info` module in Ansible is designed to gather information about
Helm plugins installed within a Kubernetes namespace. It requires the Helm tool and
can retrieve details about specific plugins if specified. Below are examples
demonstrating how to use this module with the `resources` variable pattern within
the Ansible role `c2platform.core.kubernetes`.

The first example shows how to gather information for all installed Helm plugins:

```yaml
kubernetes_resources:
  - name: Gather all Helm plugin information
    module: helm_plugin_info
```

The next example demonstrates how to gather information for a specific Helm plugin
named `env`:

```yaml
kubernetes_resources:
  - name: Gather specific Helm plugin information
    module: helm_plugin_info
    plugin_name: env
```

In this last example, we demonstrate the use of `defaults` if you commonly have a
fixed `binary_path` or API configuration across different plugin queries:

```yaml
helm_resources:
  plugins:
    - name: Gather Helm plugin information with defaults
      module: helm_plugin_info
      defaults:
        binary_path: /usr/local/bin/helm
      resources:
        - plugin_name: env
        - plugin_name: another_plugin
```

For more detailed documentation on the `kubernetes.core.helm_plugin_info` module,
please refer to
[`kubernetes.core.helm_plugin_info`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/helm_plugin_info_module.html).

#### `helm_repository`: Manage Helm repositories.

The `helm_repository` module in Ansible manages Helm chart repositories. It provides
functionalities to add, remove, and configure repositories for Helm charts, thus enabling
seamless Helm package management. The module supports various connection options,
including API keys and certificates, to ensure secure operations. Below are some example
configurations demonstrating its usage within the `c2platform.core.kubernetes` role.

Here is an example configuration that showcases using this module to add Helm repositories:

```yaml
kubernetes_resources:
  helm_repositories:
    - name: Add stable repository
      module: helm_repository
      repo_name: stable
      repo_url: https://kubernetes.github.io/ingress-nginx
    - name: Add Red Hat Helm charts repository
      module: helm_repository
      repo_name: redhat-charts
      repo_url: https://redhat-developer.github.com/redhat-helm-charts
```

For more detailed documentation on the `kubernetes.core.helm_repository` module, please refer to
[`kubernetes.core.helm_repository`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/helm_repository_module.html).

#### `helm_template`: Render chart templates

The `helm_template` module allows you to render Helm chart templates either
to a specified output directory or as concatenated YAML documents. This module
provides flexibility by supporting chart references using repo prefixes, packaged
chart paths, paths to unpacked chart directories, and absolute URLs. Below are
some examples demonstrating the use of the `helm_template` module to render
templates with different configurations.

Here is an example configuration that demonstrates rendering chart templates
for Prometheus to a specified directory:

```yaml
kubernetes_resources:
  - name: Render Prometheus templates to specified directory
    module: helm_template
    chart_ref: stable/prometheus
    output_dir: mycharts
```

Using `defaults` and `resources`, we can DRY up the configuration for rendering
templates for multiple charts:

```yaml
kubernetes_resources:
  helm_templates:
    - name: Render templates for different charts
      module: helm_template
      defaults:
        output_dir: mycharts
      resources:
        - chart_ref: stable/prometheus
        - chart_ref: istio/istiod
          chart_version: "1.13.0"
          release_namespace: "istio-system"
          show_only:
            - "templates/revision-tags.yaml"
          release_values:
            revision: "1-13-0"
            revisionTags:
              - "canary"
```

For more detailed documentation on the `kubernetes.core.helm_template` module, please refer to
[`kubernetes.core.helm_template`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/helm_template_module.html).

#### `k8s`: Manage Kubernetes (K8s) objects

The `k8s` module in Ansible allows users to manage Kubernetes objects using the
Kubernetes Python client, supporting CRUD operations across a wide range of K8s APIs.
It provides flexible methods to define objects either inline, through files, templates
or via definitions while ensuring secure operations through various forms of
authentication. Below are some example configurations utilizing the `k8s` module to
showcase its capabilities.

```yaml
kubernetes_resources:
  - name: Create a Kubernetes namespace
    module: k8s
    api_version: v1
    kind: Namespace
    state: present
    name: testing

  - name: Create a Service object with an inline definition
    module: k8s
    state: present
    resource_definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: web
        namespace: testing
        labels:
          app: galaxy
          service: web
      spec:
        selector:
          app: galaxy
          service: web
        ports:
          - protocol: TCP
            targetPort: 8000
            name: port-8000-tcp
            port: 8000

  - name: Remove an existing Service object
    module: k8s
    state: absent
    api_version: v1
    kind: Service
    namespace: testing
    name: web

  namespaces:
    - name: Batch create namespaces using defaults
      module: k8s
      defaults:
        api_version: v1
        kind: Namespace
        state: present
      resources:
        - name: first-namespace
        - name: second-namespace
        - name: third-namespace
```

For more detailed documentation on the `k8s` module, please refer to
[`kubernetes.core.k8s`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_module.html).

#### `k8s_cluster_info`: Describe Kubernetes (K8s) cluster, APIs available and their respective versions

The `k8s_cluster_info` module in Ansible is utilized to perform read operations on Kubernetes cluster objects. It authenticates using various methods like config files, certificates, passwords, or tokens. Here's how you can use this module with the `kubernetes_resources` variable to gather information without invalidating the cache:

```yaml
kubernetes_resources:
  cluster_info:
    - name: Gather Cluster Information
      module: k8s_cluster_info
      invalidate_cache: false
      register: api_status
```

This example illustrates how to structure the playbook to use the resources variable appropriately. For more detailed documentation on the `k8s_cluster_info` module, please refer to
[`kubernetes.core.k8s_cluster_info`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_cluster_info_module.html).

#### `k8s_cp`: Copy files and directories to and from pod.

The `k8s_cp` module in Ansible allows you to copy files and directories to and
from containers within a pod using the Kubernetes Python client. It's a
convenient way to manage data transfer between your local environment and
containerized applications running in Kubernetes. Below are some example
configurations showcasing how to use this module effectively:

```yaml
kubernetes_resources:
  - name: Copy a file from local to a pod
    module: k8s_cp
    namespace: some-namespace
    pod: some-pod
    remote_path: /tmp/bar
    local_path: /tmp/foo
  - name: Copy a directory from local to a pod
    module: k8s_cp
    namespace: some-namespace
    pod: some-pod
    remote_path: /tmp/bar_dir
    local_path: /tmp/foo_dir
  - name: Copy a file to a specific container in a pod
    module: k8s_cp
    namespace: some-namespace
    pod: some-pod
    container: some-container
    remote_path: /tmp/bar
    local_path: /tmp/foo
    no_preserve: True
    state: to_pod
  - name: Copy a file from a pod to local
    module: k8s_cp
    namespace: some-namespace
    pod: some-pod
    remote_path: /tmp/foo
    local_path: /tmp/bar
    state: from_pod
  - name: Copy content directly into a file in the pod
    module: k8s_cp
    state: to_pod
    namespace: some-namespace
    pod: some-pod
    remote_path: /tmp/foo.txt
    content: "This content will be copied into remote file"
```

For more detailed documentation on the `kubernetes.core.k8s_cp` module, please refer to
[`kubernetes.core.k8s_cp`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_cp_module.html).

#### `k8s_drain`: Drain, Cordon, or Uncordon node in k8s cluster

The `k8s_drain` module is used to manage the state of nodes in a Kubernetes
cluster, allowing you to drain, cordon, or uncordon nodes in preparation for
maintenance or scheduling changes. Draining a node involves marking the node as
unschedulable and evicting or deleting pods, while cordoning and uncordoning alter
the schedulability of the node.

Below are some example configurations illustrating the use of the `k8s_drain`
module.

```yaml
kubernetes_resources:
  nodes:
    - name: Drain node foo with force
      module: k8s_drain
      state: drain
      name: foo
      force: yes

    - name: Drain node foo with a grace period
      module: k8s_drain
      state: drain
      name: foo
      delete_options:
        terminate_grace_period: 900

    - name: Uncordon node foo
      module: k8s_drain
      state: uncordon
      name: foo

    - name: Cordon node foo
      module: k8s_drain
      state: cordon
      name: foo
```

For more detailed documentation on the `kubernetes.core.k8s_drain` module, please refer to
[`kubernetes.core.k8s_drain`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_drain_module.html).

#### `k8s_exec`: Execute command in Pod

The `k8s_exec` module enables command execution inside a Kubernetes Pod using the Kubernetes
Python client. It is suitable for running commands such as initialization, app
management, or status checks within containers. Below are examples demonstrating
its practical use:

Here's a basic example to execute a command in a pod:

```yaml
kubernetes_resources:
  - name: Execute a command
    module: k8s_exec
    namespace: myproject
    pod: zuul-scheduler
    command: zuul-scheduler full-reconfigure
```

To check the return code of a command executed:

```yaml
kubernetes_resources:
  - name: Check RC status of command executed
    module: k8s_exec
    namespace: myproject
    pod: busybox-test
    command: cmd_with_non_zero_exit_code
    register: command_status
    ignore_errors: True

  - name: Check last command status
    module: debug
    msg: "cmd failed"
    when: command_status.rc != 0
```

Executing a command targeting a specific container:

```yaml
kubernetes_resources:
  - name: Specify a container name to execute the command
    module: k8s_exec
    namespace: myproject
    pod: busybox-test
    container: manager
    command: echo "hello"
```

For more detailed documentation on the `kubernetes.core.k8s_exec` module,
please refer to
[`kubernetes.core.k8s_exec`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_exec_module.html).

#### `k8s_info`: Describe Kubernetes (K8s) objects

The `k8s_info` module allows you to perform read operations on Kubernetes objects
using the Kubernetes Python client. It's effective for exploring and retrieving
information about K8s resources utilizing various authentication methods and supporting
the complete range of Kubernetes APIs. Below are some examples demonstrating how to
use this module to gather information regarding Kubernetes objects in a structured
and efficient manner.

Here's how you can interact with this module using the "resources" variable format:

```yaml
kubernetes_resources:
  - name: Get an existing Service object
    module: k8s_info
    api_version: v1
    kind: Service
    name: web
    namespace: testing
    register: web_service

  - name: Get a list of all service objects
    module: k8s_info
    api_version: v1
    kind: Service
    namespace: testing
    register: service_list

  - name: Search for all Pods labelled app=web
    module: k8s_info
    kind: Pod
    label_selectors:
      - app = web
      - tier in (dev, test)
```

For more examples, you can leverage `defaults` and `resources` to make the configuration
cleaner:

```yaml
kubernetes_resources:
  pods_info:
    - name: Fetch pod information
      module: k8s_info
      defaults:
        kind: Pod
      resources:
        - label_selectors:
            - "app = {{ app_label_web }}"
          vars:
            app_label_web: web
        - field_selectors:
            - status.phase=Running
```

For more detailed documentation on the `kubernetes.core.k8s_info` module, please refer to
[`kubernetes.core.k8s_info`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_info_module.html).

#### `k8s_json_patch`: Apply JSON patch operations to existing objects

The `k8s_json_patch` module in Ansible is designed to apply RFC 6902 JSON patch
operations to existing Kubernetes objects. It is specifically used for patching
rather than strategic or JSON merge patch operations, enabling precise control
over changes to Kubernetes resources. Below are examples demonstrating how to
use this module to apply multiple JSON patch operations to a Pod:

```yaml
kubernetes_resources:
  patches:
    - name: Apply patch to a Pod
      module: k8s_json_patch
      kind: Pod
      namespace: testing
      name: mypod
      patch:
        - op: add
          path: /metadata/labels/app
          value: myapp
        - op: replace
          path: /spec/containers/0/image
          value: nginx
```

For more detailed documentation on the `kubernetes.core.k8s_json_patch` module,
please refer to
[`kubernetes.core.k8s_json_patch`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_json_patch_module.html).

#### `k8s_log`: Fetch logs from Kubernetes resources

The `k8s_log` module in Ansible allows users to fetch logs from Kubernetes
resources using the Kubernetes Python client. This module provides flexibility
in authentication methods, including the use of a config file, certificates,
passwords, or tokens. It parallels the `kubectl logs` or `oc logs` commands
in functionality and supports check mode.

Below are some examples of how to use the `k8s_log` module within the
`c2platform.core.kubernetes` role with the `kubernetes_resources` variable.

```yaml
# Example: Get a single log from a Pod
kubernetes_resources:
  logs:
    - name: Fetch log from a specific Pod
      module: k8s_log
      name: example-1
      namespace: testing

# Example: Fetch logs from a Pod using a label selector
kubernetes_resources:
  logs:
    - name: Fetch log from pod with label selector
      module: k8s_log
      namespace: testing
      label_selectors:
        - app=example

# Example: Fetch logs from a Deployment
kubernetes_resources:
  logs:
    - name: Fetch log from a Deployment
      module: k8s_log
      api_version: apps/v1
      kind: Deployment
      namespace: testing
      name: example
      since_seconds: "4000"

# Example: Fetch logs from a DeploymentConfig
kubernetes_resources:
  logs:
    - name: Fetch log from a DeploymentConfig
      module: k8s_log
      api_version: apps.openshift.io/v1
      kind: DeploymentConfig
      namespace: testing
      name: example
```

For more detailed documentation on the `kubernetes.core.k8s_log` module,
please refer to [`kubernetes.core.k8s_log`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_log_module.html).

#### `k8s_rollback`: Rollback Kubernetes (K8S) Deployments and DaemonSets

The `k8s_rollback` module in Ansible provides functionality to rollback
Kubernetes Deployments and DaemonSets, utilizing the Kubernetes Python client.
It supports authentication through various methods such as config file,
certificates, password, or token, and is analogous to the `kubectl rollout
undo` command. Below are some examples demonstrating how to use this module.

```yaml
kubernetes_resources:
  deployments:
    - name: Rollback a failed web deployment
      module: k8s_rollback
      api_version: apps/v1
      kind: Deployment
      name: web
      namespace: testing

  daemonsets:
    - name: Rollback a failed daemonset
      module: k8s_rollback
      api_version: apps/v1
      kind: DaemonSet
      name: fluentd
      namespace: logging
```

For configurations requiring multiple objects to rollback, leveraging the
grouping feature can be beneficial:

```yaml
kubernetes_resources:
  - name: Rollback multiple resources
    module: k8s_rollback
    resources:
      - api_version: apps/v1
        kind: Deployment
        name: web
        namespace: testing
      - api_version: apps/v1
        kind: DaemonSet
        name: fluentd
        namespace: logging
```

For more detailed documentation on the `kubernetes.core.k8s_rollback` module, please refer to
[`kubernetes.core.k8s_rollback`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_rollback_module.html).

#### `k8s_scale`: Set a new size for a Deployment, ReplicaSet, Replication Controller, or Job.

The `k8s_scale` module in Ansible is used to manage the number of replicas for a
Deployment, ReplicaSet, Replication Controller, or the parallelism attribute of a Job
in Kubernetes. This can be particularly useful for quickly adjusting the scale of your
applications to meet demand.

Below are examples of how to use the `k8s_scale` module within a `kubernetes_resources`
variable, which is part of the `c2platform.core.kubernetes` role:

```yaml
kubernetes_resources:
  - name: Scale deployment up and extend timeout
    module: k8s_scale
    api_version: v1
    kind: Deployment
    name: elastic
    namespace: myproject
    replicas: 3
    wait_timeout: 60

  - name: Scale deployment down when current replicas match
    module: k8s_scale
    api_version: v1
    kind: Deployment
    name: elastic
    namespace: myproject
    current_replicas: 3
    replicas: 2

  - name: Increase job parallelism
    module: k8s_scale
    api_version: batch/v1
    kind: Job
    name: pi-with-timeout
    namespace: testing
    replicas: 2

  - name: Scale deployment using label selectors
    module: k8s_scale
    replicas: 3
    kind: Deployment
    namespace: test
    label_selectors:
      - app=test
    continue_on_error: true
```

For more detailed documentation on the `kubernetes.core.k8s_scale` module, please refer to
[`kubernetes.core.k8s_scale`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_scale_module.html).

#### `k8s_service`: Manage Services on Kubernetes

The `k8s_service` module manages Services in a Kubernetes cluster, allowing users to
expose their applications to network access as desired. This module supports
various service types such as `ClusterIP`, `NodePort`, `LoadBalancer`, and `ExternalName`.
Below are some examples demonstrating the use of this module to create and manage
Kubernetes Services.

```yaml
kubernetes_resources:
  services:
    - name: Expose https port with ClusterIP
      module: k8s_service
      state: present
      name: test-https
      namespace: default
      ports:
        - port: 443
          protocol: TCP
      selector:
        key: special
```

To explore another aspect, here is how to define a Service using a YAML spec:

```yaml
kubernetes_resources:
  services:
    - name: Expose https port with ClusterIP using spec
      module: k8s_service
      state: present
      name: test-https
      namespace: default
      resource_definition:
        spec:
          ports:
            - port: 443
              protocol: TCP
          selector:
            key: special
```

For more detailed documentation on the `kubernetes.core.k8s_service` module, please refer to
[`kubernetes.core.k8s_service`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_service_module.html).

#### `k8s_taint`: Taint a node in a Kubernetes/OpenShift cluster

The `k8s_taint` module allows you to manage taints on Kubernetes nodes, ensuring
that only pods with specific tolerations can be scheduled onto those nodes. This
module provides functionality to both add and remove taints on nodes in a
Kubernetes/OpenShift cluster.

Below are examples illustrating how to use the `k8s_taint` module with the
`kubernetes_resources` variable in role `c2platform.core.kubernetes`.

```yaml
kubernetes_resources:
  taints:
    - name: Taint node "foo"
      module: k8s_taint
      state: present
      defaults:
        name: foo
      resources:
        - taints:
            - effect: NoExecute
              key: "key1"
            - effect: NoSchedule
              key: "key1"
              value: "value1"
    - name: Remove taint from "foo"
      module: k8s_taint
      state: absent
      name: foo
      taints:
        - effect: NoExecute
          key: "key1"
          value: "value1"
```

For more detailed documentation on the `kubernetes.core.k8s_taint` module,
please refer to
[`kubernetes.core.k8s_taint`](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_taint_module.html).

#### `reboot`: Reboot a machine

The `reboot` module in Ansible is used to reboot a machine, wait for it to go down,
come back up, and then respond to commands. It is specifically designed for POSIX
platforms; for Windows targets, the `ansible.windows.win_reboot` module should be
used. Below are some examples demonstrating the use of this module within the
`c2platform.core.kubernetes` role using the `kubernetes_resources` variable.

```yaml
kubernetes_resources:
  - name: Unconditionally reboot the machine with all defaults
    module: reboot

  - name: Reboot a slow machine that might have lots of updates to apply
    module: reboot
    reboot_timeout: 3600

  - name: Reboot a machine with shutdown command in an unusual place
    module: reboot
    search_paths:
      - '/lib/molly-guard'

  - name: Reboot machine using a custom reboot command
    module: reboot
    reboot_command: launchctl reboot userspace
    boot_time_command: uptime | cut -d ' ' -f 5
```

For more detailed documentation on the `ansible.builtin.reboot` module, please refer to
[`ansible.builtin.reboot`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/reboot_module.html).

#### `script`: Runs a local script on a remote node after transferring it

The `script` module in Ansible is used to run local scripts on remote nodes. It first
transfers the script to the remote node, then executes it. The module supports both
free-form command execution and the use of the `cmd` parameter. Below are examples
illustrating how the module can be used in conjunction with the c2platform.core.kubernetes
role by utilizing the `script_resources` variable.

```yaml
script_resources:
  scripts:
    - name: Run a simple script
      module: script
      cmd: /some/local/script.sh --some-argument 1234

    - name: Create file if it doesn't exist
      module: script
      cmd: /some/local/create_file.sh --some-argument 1234
      creates: /the/created/file.txt

    - name: Remove file if it exists
      module: script
      cmd: /some/local/remove_file.sh --some-argument 1234
      removes: /the/removed/file.txt

    - name: Run script with a specific executable
      module: script
      cmd: /some/local/script.py
      executable: python3
```

For more detailed documentation on the `ansible.builtin.script` module, please refer to
[`ansible.builtin.script`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/script_module.html).

#### `shell`: Execute shell commands on targets

The `shell` module in Ansible allows you to execute shell commands on remote
targets. This module is particularly useful when you need to use shell
features like pipes, redirection, and environment variables that are not
available with the `command` module. The `shell` module executes commands
through a shell (/bin/sh) on a remote node, making it flexible for various
scripting needs.

Below is an example configuration that demonstrates using this module to run
a script and handle directory navigation and file creation:

```yaml
shell_resources:
  scripts:
    - name: Execute script and log the output
      module: shell
      defaults:
        chdir: somedir/
        creates: somedir/somelog.txt
      resources:
        - cmd: somescript.sh >> somelog.txt
```

Here's another example showing how to change the shell executable and handle
complex commands requiring a different shell:

```yaml
shell_resources:
  advanced:
    - name: Run a command using bash for specific shell features
      module: shell
      resources:
        - cmd: cat < /tmp/*txt
        executable: /bin/bash
```

For more detailed documentation on the `ansible.builtin.shell` module, please refer to
[`ansible.builtin.shell`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html).

#### `uri`: Interacts with webservices

The `uri` module allows for interaction with HTTP and HTTPS web services. It supports
different authentication methods, provides the ability to follow redirects, and
can handle both GET and POST requests among many others. Below are some examples
demonstrating the use of the `uri` module using the `kubernetes_resources` variable.

Example of a simple GET request to check if a webpage can be accessed and returns
a status code of 200:

```yaml
kubernetes_resources:
  uri_requests:
    - name: Check connection to a webpage
      module: uri
      url: http://www.example.com
    resources:
      - url: http://www.example.com
        status_code: [200]
  ```

Example of using POST method to create a new JIRA issue by sending JSON data via
HTTP Basic Authentication:

```yaml
kubernetes_resources:
  uri_requests:
    - name: Create a JIRA issue
      module: uri
      defaults:
        url: https://your.jira.example.com/rest/api/2/issue/
        user: your_username
        password: your_pass
        method: POST
        force_basic_auth: true
        status_code: [201]
        body_format: json
      resources:
        - body: "{{ lookup('file', 'issue.json') }}"
```

Example of logging into a form-based webpage using form-urlencoded body format and
capturing the returned cookie for future use:

```yaml
kubernetes_resources:
  uri_requests:
    - name: Login to a form-based page
      module: uri
      defaults:
        url: https://your.form.based.auth.example.com/index.php
        method: POST
        body_format: form-urlencoded
        status_code: [302]
      resources:
        - body:
            name: your_username
            password: your_password
            enter: Sign in
      register: login

    - name: Access app using stored cookie
      module: uri
      defaults:
        url: https://your.form.based.auth.example.com/dashboard.php
        method: GET
        return_content: true
      resources:
        - headers:
            Cookie: "{{ login.cookies_string }}"
```

For more detailed documentation on the `ansible.builtin.uri` module, please refer to
[`ansible.builtin.uri`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/uri_module.html).

#### `wait_for_connection`: Waits until remote system is reachable/usable

The `wait_for_connection` module in Ansible is used to ensure that a remote
system is reachable and usable before proceeding with any further tasks. It
retries the transport connection after a timeout of `connect_timeout` seconds
and tests the transport connection every `sleep` seconds until the total
`timeout` duration is reached. Below are example configurations that
demonstrate using this module with some adjustments.

Here's a simple setup to wait for a system to become reachable:

```yaml
kubernetes_resources:
  wait_for_connections:
    - name: Simple wait for system reachability
      module: ansible.builtin.wait_for_connection
```

You can also start checking after a specific delay:

```yaml
kubernetes_resources:
  wait_for_connections:
    - name: Wait with a delay before checking
      module: ansible.builtin.wait_for_connection
      delay: 60
      timeout: 300
```

If you're managing connections for multiple systems, the `defaults` and
`resources` keys can be used for DRY configurations:

```yaml
kubernetes_resources:
  wait_for_connections:
    - name: Managing multiple system connections
      module: ansible.builtin.wait_for_connection
      defaults:
        connect_timeout: 10
        sleep: 2
        timeout: 600
      resources:
        - name: System 1
        - name: System 2
        - name: System 3
```

For more detailed documentation on the `ansible.builtin.wait_for_connection`
module, please refer to
[`ansible.builtin.wait_for_connection`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_connection_module.html).

<!-- end supported_modules -->

### `kubernetes_linux_resources` and `kubernetes_linux_bootstrap_resources`

Additional configuration is achieved via the versatile
`kubernetes_linux_resources` and `kubernetes_linux_bootstrap_resources`
variable. These variable integrate the `c2platform.core.linux` role, allowing
management of a wide range of resources using various Ansible modules. For
detailed information, refer to the
[documentation](https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/linux)
of this role and see the [Example Playbook](#example-playbook).

The difference between the two is the order of processing.

1. First `kubernetes_linux_bootstrap_resources` is processed.
2. Then `kubernetes_resources` is procssed.
3. Finaly  `kubernetes_linux_resources` is processed.

The example below shows installation of ArgoCD CLI

```yaml
c2_argocd_cli_tag: v2.13.3
kubernetes_linux_resources:
  argocd_cli:
    - name: Download & Install ArgoCD CLI
      type: shell
      cmd: |
        curl -sSL -o argocd-linux-amd64 curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/download/{{ c2_argocd_cli_tag }}/argocd-linux-amd64
        install -m 555 argocd-linux-amd64 /usr/local/bin/argocd-{{ c2_argocd_cli_tag }}
        rm argocd-linux-amd64
      creates: "/usr/local/bin/argocd-{{ c2_argocd_cli_tag }}"
    - name: /usr/local/bin/argocd
      type: file
      src: "/usr/local/bin/argocd-{{ c2_argocd_cli_tag }}"
      state: link
```

### Type `service_uri`

The `service_uri`type is a specialized Ansible task used to wait until a Kubernetes service becomes available. It is particularly useful when you need to ensure that a service is fully operational before proceeding with further provisioning tasks. This is common when deploying applications that require services to be up and running, such as the AWX operator for AWX installation as described in
[Setup the Automation Controller ( AWX ) using Ansible | C2 Platform](https://c2platform.org/en/docs/howto/awx/awx/)

You can include an item of type service_uri in your `kubernetes_config` to specify the service you want to wait for. Here's an example of how to configure it:

```yaml
kubernetes_config:
  - summary: Wait awx_service
    type: service_uri
    name: awx_service
    namespace: awx
    util_content: AWX REST API
    url_path: api/
    url_username: admin
    url_password: secret
    delay: 30
    retries: 100
```

A `kubernetes_config` of type `service_uri` will be processed as follows:

1. **Waiting for Service IP Assignment:** Ansible will use the
   `kubernetes.core.k8s_info` module to wait until the `awx_service` in the
   specified namespace is assigned an IP address.
2. **Service Availability Check:** Once an IP address is available, Ansible will
   repeatedly query the service using the `ansible.builtin.uri` module. It will
   continue to retry until the service responds with an HTTP status code of
   `200`, indicating a successful connection. Additionally, Ansible will check
   that the content returned by the service at the specified URL contains the
   text "AWX REST API."

The `service_uri` type can be used to ensure that your automation workflow will
pause until a service is fully deployed and ready to accept requests,
guaranteeing a smoother and more reliable deployment process.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See [plays/mw/mk8s.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mw/mk8s.yml) and [group_vars/mk8s/kubernetes.yml](https://gitlab.com/c2platform/ansible/-/tree/master/group_vars/mk8s/kubernetes.yml)

