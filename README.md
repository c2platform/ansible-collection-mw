# Ansible Collection - c2platform.mw

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-mw/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-mw/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/ansible-collection-mw/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-mw/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/mw/)

C2 Platform middleware roles

## Roles

* [apache](./roles/apache)
* [docker](./roles/docker)
* [haproxy](./roles/haproxy)
* [nfs](./roles/nfs)
* [postgresql](./roles/postgresql)
* [proxy](./roles/proxy)
* [tomcat](./roles/tomcat)
* [keycloak](./roles/keycloak)
* [dnsmasq](./roles/dnsmasq)
* [microk8s](./roles/microk8s)
* [kubernetes](./roles/kubernetes)
* [exim4](./roles/exim4)

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/mw/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.mw
ansible-doc -t filter --list c2platform.mw
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
