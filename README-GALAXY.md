# Ansible Collection - c2platform.mw

[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-mw/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-mw/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-mw/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-mw/-/pipelines)

See [README](https://gitlab.com/c2platform/ansible-collection-mw/-/blob/master/README.md).
